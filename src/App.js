
import './App.css';
import React from 'react'
class Game extends React.Component {
 constructor(props) {
   super(props)
  this.state = {
    xwins:0,
    owins:0,
    ties:0,
    x_turn: true,
      i_1: '',
      i_2: '',
      i_3: '',
      i_4: '',
      i_5: '',
      i_6: '',
      i_7: '',
      i_8: '',
      i_9: ''
  }
 }
 componentDidMount = () => {
   const scores = JSON.parse(localStorage.getItem('scores'))
   if(scores){
   this.setState({ xwins: scores.x, owins: scores.o, ties: scores.t })
   }
 }

 handleChange = e => {
   if(document.getElementById(e.target.id).disabled === true) {
     return
   }
   const { x_turn } = this.state
  if(x_turn) {
    this.setState({ [e.target.name]: 'X' })
   document.getElementById(e.target.id).disabled = true 
  }
  else {
    this.setState({ [e.target.name]: '0' })
   document.getElementById(e.target.id).disabled = true 
  }
 setTimeout(() => {
  this.checkWinner()
 }, 100);
}
checkWinner() {
  const { x_turn } = this.state
  let i_1 = document.getElementById("1").value;
  let i_2 = document.getElementById("2").value;
  let i_3 = document.getElementById("3").value;
  let i_4 = document.getElementById("4").value;
  let i_5 = document.getElementById("5").value;
  let i_6 = document.getElementById("6").value;
  let i_7 = document.getElementById("7").value;
  let i_8 = document.getElementById("8").value;
  let i_9 = document.getElementById("9").value;

  if (i_1.toLowerCase() === 'x' && i_2.toLowerCase() === 'x' && i_3.toLowerCase() === 'x') {
    localStorage.setItem('scores', JSON.stringify({x: this.state.xwins + 1, o: this.state.owins, t: this.state.ties}))
        this.setState({ xwins: this.state.xwins + 1})
        document.getElementById("1").style.backgroundColor = 'green';
        document.getElementById("2").style.backgroundColor = 'green';
        document.getElementById("3").style.backgroundColor = 'green';
        document.getElementById("4").disabled = true;
        document.getElementById("5").disabled = true;
        document.getElementById("6").disabled = true;
        document.getElementById("7").disabled = true;
        document.getElementById("8").disabled = true;
        document.getElementById("9").disabled = true;
    }
    else if (i_1.toLowerCase() === 'x' && i_4.toLowerCase() === 'x' && i_7.toLowerCase() === 'x') {
      localStorage.setItem('scores', JSON.stringify({x: this.state.xwins + 1, o: this.state.owins, t: this.state.ties}))
      this.setState({ xwins: this.state.xwins + 1})
      document.getElementById("1").style.backgroundColor = 'green';
      document.getElementById("4").style.backgroundColor = 'green';
      document.getElementById("7").style.backgroundColor = 'green';
        document.getElementById("2").disabled = true;
        document.getElementById("3").disabled = true;
        document.getElementById("5").disabled = true;
        document.getElementById("6").disabled = true;
        document.getElementById("8").disabled = true;
        document.getElementById("9").disabled = true;
    }
    else if (i_7.toLowerCase() === 'x' && i_8.toLowerCase() === 'x' && i_9.toLowerCase() === 'x') {
      localStorage.setItem('scores', JSON.stringify({x: this.state.xwins + 1, o: this.state.owins, t: this.state.ties}))
      this.setState({ xwins: this.state.xwins + 1})
      document.getElementById("7").style.backgroundColor = 'green';
      document.getElementById("8").style.backgroundColor = 'green';
      document.getElementById("9").style.backgroundColor = 'green';
        document.getElementById("1").disabled = true;
        document.getElementById("2").disabled = true;
        document.getElementById("3").disabled = true;
        document.getElementById("4").disabled = true;
        document.getElementById("5").disabled = true;
        document.getElementById("6").disabled = true;
    }
    else if  (i_3.toLowerCase() === 'x' && i_9.toLowerCase() === 'x' && i_6.toLowerCase() === 'x') {
      localStorage.setItem('scores', JSON.stringify({x: this.state.xwins + 1, o: this.state.owins, t: this.state.ties}))
      this.setState({ xwins: this.state.xwins + 1})
      document.getElementById("3").style.backgroundColor = 'green';
      document.getElementById("9").style.backgroundColor = 'green';
      document.getElementById("6").style.backgroundColor = 'green';
        document.getElementById("1").disabled = true;
        document.getElementById("2").disabled = true;
        document.getElementById("4").disabled = true;
        document.getElementById("5").disabled = true;
        document.getElementById("7").disabled = true;
        document.getElementById("8").disabled = true;
    }
    else if  (i_1.toLowerCase() === 'x' && i_5.toLowerCase() === 'x' && i_9.toLowerCase() === 'x') {
      localStorage.setItem('scores', JSON.stringify({x: this.state.xwins + 1, o: this.state.owins, t: this.state.ties}))
      this.setState({ xwins: this.state.xwins + 1})
      document.getElementById("1").style.backgroundColor = 'green';
      document.getElementById("5").style.backgroundColor = 'green';
      document.getElementById("9").style.backgroundColor = 'green';
        document.getElementById("2").disabled = true;
        document.getElementById("3").disabled = true;
        document.getElementById("4").disabled = true;
        document.getElementById("6").disabled = true;
        document.getElementById("7").disabled = true;
        document.getElementById("8").disabled = true;
    }
    else if  (i_3.toLowerCase() === 'x' && i_5.toLowerCase() === 'x' && i_7.toLowerCase() === 'x') {
      localStorage.setItem('scores', JSON.stringify({x: this.state.xwins + 1, o: this.state.owins, t: this.state.ties}))
      this.setState({ xwins: this.state.xwins + 1})
      document.getElementById("3").style.backgroundColor = 'green';
      document.getElementById("5").style.backgroundColor = 'green';
      document.getElementById("7").style.backgroundColor = 'green';
        document.getElementById("1").disabled = true;
        document.getElementById("2").disabled = true;
        document.getElementById("4").disabled = true;
        document.getElementById("6").disabled = true;
        document.getElementById("8").disabled = true;
        document.getElementById("9").disabled = true;
    }
    else if  (i_2.toLowerCase() === 'x' && i_5.toLowerCase() === 'x' && i_8.toLowerCase() === 'x'){
      localStorage.setItem('scores', JSON.stringify({x: this.state.xwins + 1, o: this.state.owins, t: this.state.ties}))
      this.setState({ xwins: this.state.xwins + 1})
      document.getElementById("2").style.backgroundColor = 'green';
      document.getElementById("5").style.backgroundColor = 'green';
      document.getElementById("8").style.backgroundColor = 'green';
        document.getElementById("1").disabled = true;
        document.getElementById("3").disabled = true;
        document.getElementById("4").disabled = true;
        document.getElementById("6").disabled = true;
        document.getElementById("7").disabled = true;
        document.getElementById("9").disabled = true;
    }
    else if (i_4.toLowerCase() === 'x' && i_5.toLowerCase() === 'x' && i_6.toLowerCase() === 'x') {
      localStorage.setItem('scores', JSON.stringify({x: this.state.xwins + 1, o: this.state.owins, t: this.state.ties}))
      this.setState({ xwins: this.state.xwins + 1})
      document.getElementById("4").style.backgroundColor = 'green';
      document.getElementById("5").style.backgroundColor = 'green';
      document.getElementById("6").style.backgroundColor = 'green';
        document.getElementById("1").disabled = true;
        document.getElementById("2").disabled = true;
        document.getElementById("3").disabled = true;
        document.getElementById("7").disabled = true;
        document.getElementById("8").disabled = true;
        document.getElementById("9").disabled = true;
    }
    else  if (i_1.toLowerCase() === '0' && i_2.toLowerCase() === '0' && i_3.toLowerCase() === '0') {
      localStorage.setItem('scores', JSON.stringify({x: this.state.xwins, o: this.state.owins + 1, t: this.state.ties}))
      this.setState({ owins: this.state.owins + 1})
      document.getElementById("1").style.backgroundColor = 'green';
      document.getElementById("2").style.backgroundColor = 'green';
      document.getElementById("3").style.backgroundColor = 'green';
      document.getElementById("4").disabled = true;
      document.getElementById("5").disabled = true;
      document.getElementById("6").disabled = true;
      document.getElementById("7").disabled = true;
      document.getElementById("8").disabled = true;
      document.getElementById("9").disabled = true;
  }
  else if (i_1.toLowerCase() === '0' && i_4.toLowerCase() === '0' && i_7.toLowerCase() === '0') {
    localStorage.setItem('scores', JSON.stringify({x: this.state.xwins, o: this.state.owins + 1, t: this.state.ties}))
    this.setState({ owins: this.state.owins + 1})
    document.getElementById("1").style.backgroundColor = 'green';
    document.getElementById("4").style.backgroundColor = 'green';
    document.getElementById("7").style.backgroundColor = 'green';
      document.getElementById("2").disabled = true;
      document.getElementById("3").disabled = true;
      document.getElementById("5").disabled = true;
      document.getElementById("6").disabled = true;
      document.getElementById("8").disabled = true;
      document.getElementById("9").disabled = true;
  }
  else if (i_7.toLowerCase() === '0' && i_8.toLowerCase() === '0' && i_9.toLowerCase() === '0') {
    localStorage.setItem('scores', JSON.stringify({x: this.state.xwins, o: this.state.owins + 1, t: this.state.ties}))
    this.setState({ owins: this.state.owins + 1})
    document.getElementById("7").style.backgroundColor = 'green';
    document.getElementById("8").style.backgroundColor = 'green';
    document.getElementById("9").style.backgroundColor = 'green';
      document.getElementById("1").disabled = true;
      document.getElementById("2").disabled = true;
      document.getElementById("3").disabled = true;
      document.getElementById("4").disabled = true;
      document.getElementById("5").disabled = true;
      document.getElementById("6").disabled = true;
  }
  else if  (i_3.toLowerCase() === '0' && i_9.toLowerCase() === '0' && i_6.toLowerCase() === '0') {
    localStorage.setItem('scores', JSON.stringify({x: this.state.xwins, o: this.state.owins + 1, t: this.state.ties}))
    this.setState({ owins: this.state.owins + 1})
    document.getElementById("3").style.backgroundColor = 'green';
    document.getElementById("9").style.backgroundColor = 'green';
    document.getElementById("6").style.backgroundColor = 'green';
      document.getElementById("1").disabled = true;
      document.getElementById("2").disabled = true;
      document.getElementById("4").disabled = true;
      document.getElementById("5").disabled = true;
      document.getElementById("7").disabled = true;
      document.getElementById("8").disabled = true;
  }
  else if  (i_1.toLowerCase() === '0' && i_5.toLowerCase() === '0' && i_9.toLowerCase() === '0') {
    localStorage.setItem('scores', JSON.stringify({x: this.state.xwins, o: this.state.owins + 1, t: this.state.ties}))
    this.setState({ owins: this.state.owins + 1})
    document.getElementById("1").style.backgroundColor = 'green';
    document.getElementById("5").style.backgroundColor = 'green';
    document.getElementById("9").style.backgroundColor = 'green';
      document.getElementById("2").disabled = true;
      document.getElementById("3").disabled = true;
      document.getElementById("4").disabled = true;
      document.getElementById("6").disabled = true;
      document.getElementById("7").disabled = true;
      document.getElementById("8").disabled = true;
  }
  else if  (i_3.toLowerCase() === '0' && i_5.toLowerCase() === '0' && i_7.toLowerCase() === '0') {
    localStorage.setItem('scores', JSON.stringify({x: this.state.xwins, o: this.state.owins + 1, t: this.state.ties}))
    this.setState({ owins: this.state.owins + 1})
    document.getElementById("3").style.backgroundColor = 'green';
    document.getElementById("5").style.backgroundColor = 'green';
    document.getElementById("7").style.backgroundColor = 'green';
      document.getElementById("1").disabled = true;
      document.getElementById("2").disabled = true;
      document.getElementById("4").disabled = true;
      document.getElementById("6").disabled = true;
      document.getElementById("8").disabled = true;
      document.getElementById("9").disabled = true;
  }
  else if  (i_2.toLowerCase() === '0' && i_5.toLowerCase() === '0' && i_8.toLowerCase() === '0'){
    localStorage.setItem('scores', JSON.stringify({x: this.state.xwins, o: this.state.owins + 1, t: this.state.ties}))
    this.setState({ owins: this.state.owins + 1})
    document.getElementById("2").style.backgroundColor = 'green';
    document.getElementById("5").style.backgroundColor = 'green';
    document.getElementById("8").style.backgroundColor = 'green';
      document.getElementById("1").disabled = true;
      document.getElementById("3").disabled = true;
      document.getElementById("4").disabled = true;
      document.getElementById("6").disabled = true;
      document.getElementById("7").disabled = true;
      document.getElementById("9").disabled = true;
  }
  else if (i_4.toLowerCase() === '0' && i_5.toLowerCase() === '0' && i_6.toLowerCase() === '0') {
    localStorage.setItem('scores', JSON.stringify({x: this.state.xwins, o: this.state.owins + 1, t: this.state.ties}))
    this.setState({ owins: this.state.owins + 1})
    document.getElementById("4").style.backgroundColor = 'green';
    document.getElementById("5").style.backgroundColor = 'green';
    document.getElementById("6").style.backgroundColor = 'green';
      document.getElementById("1").disabled = true;
      document.getElementById("2").disabled = true;
      document.getElementById("3").disabled = true;
      document.getElementById("7").disabled = true;
      document.getElementById("8").disabled = true;
      document.getElementById("9").disabled = true;
  }
    else if ((i_1 === 'X' || i_1 === '0') && (i_2 === 'X'
        || i_2 === '0') && (i_3 === 'X' || i_3 === '0') &&
        (i_4 === 'X' || i_4 === '0') && (i_5 === 'X' ||
        i_5 === '0') && (i_6 === 'X' || i_6 === '0') &&
        (i_7 === 'X' || i_7 === '0') && (i_8 === 'X' ||
        i_8 === '0') && (i_9 === 'X' || i_9 === '0')) {
          this.setState({ ties: this.state.ties + 1})
    }
    else {
      this.setState({ x_turn: !x_turn})
 }
}
newGame = () => {
  this.setState ({
    x_turn: true,
      i_1: '',
      i_2: '',
      i_3: '',
      i_4: '',
      i_5: '',
      i_6: '',  
      i_7: '',
      i_8: '',
      i_9: ''
  })
  document.getElementById("1").disabled = false;
  document.getElementById("2").disabled = false;
  document.getElementById("3").disabled = false;
  document.getElementById("4").disabled = false;
  document.getElementById("5").disabled = false;
  document.getElementById("6").disabled = false;
  document.getElementById("7").disabled = false;
  document.getElementById("8").disabled = false;
  document.getElementById("9").disabled = false;
  document.getElementById("1").style.backgroundColor = 'white';
  document.getElementById("2").style.backgroundColor = 'white';
  document.getElementById("3").style.backgroundColor = 'white';
  document.getElementById("4").style.backgroundColor = 'white';
  document.getElementById("5").style.backgroundColor = 'white';
  document.getElementById("6").style.backgroundColor = 'white';
  document.getElementById("7").style.backgroundColor = 'white';
  document.getElementById("8").style.backgroundColor = 'white';
  document.getElementById("9").style.backgroundColor = 'white';
}

clearScores = () => {
  this.setState({ xwins: 0, owins: 0, ties: 0})
  localStorage.removeItem('scores')
}
 render() {
   const { i_1, i_2, i_3, i_4, i_5, i_6, i_7, i_8, i_9, xwins, owins, ties } = this.state
  return (
    <div className="App">

      <div className="mainContainer">

        <div className="scoreBoard">
          <h3> ScoreBoard </h3>
          <hr/>
          X wins: {xwins} 0 Wins: {owins} Ties: {ties}
          <br/>
          <button onClick={this.newGame} >New Game</button>
          <button onClick={this.clearScores}>Clear Scores</button>
        </div>
        <div className="gameBoard">
          <table>
            <tbody>
            <tr>
              <td>
                <input type="text" value={i_1} readOnly onClick={this.handleChange} className="inputArea" id="1" name="i_1"></input>
              </td>
              <td>
                <input type="text" value={i_2} readOnly onClick={this.handleChange} className="inputArea" id="2" name="i_2"></input></td>
              <td>
                <input type="text" value={i_3} readOnly onClick={this.handleChange} className="inputArea" id="3" name="i_3"></input></td>
            </tr>
            <tr>
              <td>
                <input type="text" value={i_4} readOnly onClick={this.handleChange} className="inputArea" id="4" name="i_4"></input></td>
              <td>
                <input type="text" value={i_5} readOnly onClick={this.handleChange} className="inputArea" id="5" name="i_5"></input></td>
              <td>
                <input type="text" value={i_6} readOnly onClick={this.handleChange} className="inputArea" id="6" name="i_6"></input></td>
            </tr>
            <tr>
              <td>
                <input type="text" value={i_7} readOnly onClick={this.handleChange} className="inputArea" id="7" name="i_7"></input></td>
              <td>
                <input type="text" value={i_8} readOnly onClick={this.handleChange} className="inputArea" id="8" name="i_8"></input></td>
              <td>
                <input type="text" value={i_9} readOnly onClick={this.handleChange} className="inputArea" id="9" name="i_9"></input></td>
            </tr>
            </tbody>
          </table>
        </div>

      </div>

    </div>
  );
 }
}

export default Game;
